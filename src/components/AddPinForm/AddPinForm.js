
import React, {Component} from 'react';
import {connect} from 'react-redux';
import './AddPinForm.css';


class AddPinForm extends Component {
    render() {
        return (
            <div className="AddPinForm">
                <input
                    className="input"
                    type="password"
                    value={this.props.pin}
                    style={this.props.success ? {background: 'lightgreen'} : {background: 'white'}}
                />

                <div className="buttons"
                     >
                    <button onClick={() => this.props.addNumber(1)}>1</button>
                    <button onClick={() => this.props.addNumber(2)}>2</button>
                    <button onClick={() => this.props.addNumber(3)}>3</button><br/>
                    <button onClick={() => this.props.addNumber(4)}>4</button>
                    <button onClick={() => this.props.addNumber(5)}>5</button>
                    <button onClick={() => this.props.addNumber(6)}>6</button><br/>
                    <button onClick={() => this.props.addNumber(7)}>7</button>
                    <button onClick={() => this.props.addNumber(8)}>8</button>
                    <button onClick={() => this.props.addNumber(9)}>9</button><br/>
                    <button onClick={this.props.removeNumber}>&lt;</button>
                    <button onClick={() => this.props.addNumber(0)}>0</button>
                    <button onClick={this.props.CodeCheck} style={{padding: '15px 9px'}}>Enter</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        pin: state.pin,
        success: state.success
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNumber: number => dispatch({type: 'ADD_NUMBER', number}),
        removeNumber: () => dispatch({type: 'REMOVE'}),
        CodeCheck: () => dispatch({type: 'CHECK_CODE'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPinForm);