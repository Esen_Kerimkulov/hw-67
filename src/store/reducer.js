const initialState = {
    pinCode: '1122',
    pin: '',
    success: false,
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case 'ADD_NUMBER' :
            if (state.pin.length < 4) {
                return {
                    ...state,
                    pin: state.pin + action.number,
                    success: false
                };

            }

        case 'REMOVE' :
            const newPassword = state.pin.substr(0, state.pin.length - 1);
            return {
                ...state,
                pin: newPassword
            };
        case  'CHECK_CODE' :
            if (state.pin === state.pinCode) {
                return {
                    ...state,
                    success: true,
                }
            } else {
                alert('Wrong pin');
                return {
                    ...state,
                    success: false
                }
            }
        default :
            return state
    }


};


export default reducer;