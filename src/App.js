import React, { Component } from 'react';
import './App.css';
import AddPinForm from "./components/AddPinForm/AddPinForm";

class App extends Component {
  render() {
    return (
      <AddPinForm/>
    );
  }
}

export default App;
